/*
 * Jquery loader config
 */
module.exports = function (CONFIG) {
  return {
    test: require.resolve('jquery'),
    use: [
      {
        loader: 'expose-loader',
        query: 'jQuery'
      },
      {
        loader: 'expose-loader',
        query: '$'
      }
    ]
  }
};

