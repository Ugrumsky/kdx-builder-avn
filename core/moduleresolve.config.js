/*
 * Common modules resolve config file
 * add pull request if you want to add common module to project
 */

var path = require('path');
var fs = require('fs');

module.exports = {
  "bourbon": path.join(require('bourbon').includePaths[0], "_bourbon.scss"),
  "bourbon-neat": path.join(require('bourbon-neat').includePaths[0], "_neat.scss"),
  "neat-helpers": path.join(require('bourbon-neat').includePaths[0], "_neat-helpers.scss"),
  "normalize-css": path.join(require('node-normalize-scss').includePaths, "_normalize.scss"),
  "reset-css": path.join(require('node-reset-scss').includePath, "_reset.scss"),
  modernizr$: (function () {
    var modernizrLocalRC = path.resolve(process.cwd(), ".modernizrrc");
    return fs.existsSync(modernizrLocalRC) ? modernizrLocalRC : path.resolve(__dirname, './assets/.modernizrrc');
  }())
};
