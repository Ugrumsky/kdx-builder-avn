/**
 * Created by Kodix on 27.01.2017.
 */

// Common packages
var fs = require('fs');
var path = require('path');
var argv = require('yargs').argv;


// Utility packages
var _ = require('lodash');
var util = require('util');
var glob = require('glob');


// Webpack core
var webpack = require('webpack');


// Webpack constants
var NODE_ENV = require('./environment.config.js');
var COMMON_RESOLVE = require('./moduleresolve.config.js');


// Webpack plugins
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var AssetsPlugin = require('assets-webpack-plugin');
var OmitTildeWebpackPlugin = require('omit-tilde-webpack-plugin');

// PostCSS plugin
var autoprefixer = require('autoprefixer');
var autoprefixerConfig = require('./autoprefixer.config.js');


module.exports = function (CONFIG) {
  var PATHS = CONFIG.paths;

  // Get all entries in target path
  var entries = _.mapValues(_.keyBy(glob.sync(PATHS.pages + '*/*.@(js|ts)', {
    cwd: process.cwd()
  }), function (filePath) {
    var pathSplitted = filePath.split('/');
    return pathSplitted[pathSplitted.length - 2];
  }), function (entryPath) {
    return [
      "babel-polyfill",
      './' + entryPath
    ]
  });

  // Return undefined if no entries found
  if (Object.keys(entries).length == 0)
    return (void 0);


  // Add dev middleware if server is active
  if (CONFIG.devServer.active) {
    entries = _.mapValues(entries, function (entryPath) {
      return _.concat(
        entryPath,
        [
          'webpack-hot-middleware/client?reload=true'
        ],
        fs.existsSync(path.resolve(process.cwd(), entryPath[1].replace('js', 'pug'))) ? [entryPath[1].replace('js', 'pug')] : []
      )
    });

    entries.commons = [
      'webpack-hot-middleware/client?reload=true'
    ]
  }

  if (CONFIG.kdxTools) {
    if (_.isArray(entries.commons))
      entries.commons.push(path.resolve(__dirname, './scripts/kdx-helper/kdx-helper.js'));
    else
      entries.commons = [path.resolve(__dirname, './scripts/kdx-helper/kdx-helper.js')];
  }


  if (argv.debug)
    console.log('\nCompiled: Webpack entries\n', util.inspect(entries, false, null), '\n');


  // Get all folders in resources/frontend for fast resolve
  var resolveFolders = _.mapValues(_.keyBy(glob.sync(PATHS.sourceRoot + '*/', {
    cwd: process.cwd()
  }), function (filePath) {
    var pathSplitted = filePath.split('/');
    return pathSplitted[pathSplitted.length - 2];
  }), function (filePath) {
    return path.join(process.cwd(), filePath);
  });

  var aliases = _.assign(COMMON_RESOLVE, resolveFolders);
  // Add common resolve
  _.each(CONFIG.paths.resolve, function (resolveItem, name) {
    if (Object.keys(aliases).indexOf(name) != -1)
      console.log('\nWarning: Custom alias ' + name.toUpperCase() + ' overrides default.\n');
    aliases[name] = path.resolve(process.cwd(), resolveItem);
  });

  if (argv.debug || argv.showAliases)
    console.log('\nCompiled: Webpack aliases\n', util.inspect(aliases, false, null), '\n');

  // Loaders configuration
  var LOADERS =   _.flatten(_.map(glob.sync("./loaders/*.js", {
    cwd: __dirname
  }), function (filePath) {
    return require(filePath)(CONFIG);
  }));

  var sassExtractTextPlugin = new ExtractTextPlugin({
    filename: CONFIG.paths.assets.css + '[name]' + (CONFIG.paths.hashes ? '.[contenthash]' : '') + '.css',
    allChunks: true,
    disable: CONFIG.devServer.active
  });
  var sassLoader = {
    test: /\.scss$/,
    use: sassExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        {
          loader: 'css-loader',
          options: {
            sourceMap: !CONFIG.minify && !CONFIG.devServer.active,
            minimize: !CONFIG.devServer.active
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins: [
              autoprefixer({browsers: autoprefixerConfig})
            ]
          }
        },
        'resolve-url-loader',
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            data: "$env: " + NODE_ENV + ";"
          }
        }
      ]
    })
  };


  // Plugins configuration
  var templatesPaths = glob.sync(PATHS.pages + '*/*.@(pug|html)', {
    cwd: process.cwd()
  });

  var plugins = _.concat([
      sassExtractTextPlugin,
      new webpack.DefinePlugin(_.assign({
        'process.env': {
          NODE_ENV: JSON.stringify(NODE_ENV),
          PAGES: JSON.stringify(
            _.map(templatesPaths, function (filepath) {
              return _.last(path.parse(filepath).dir.split('/'));
            })
          ),
          PUBLIC_PATH: JSON.stringify(CONFIG.paths.publicPath)
        },
        KDX_TOOLS: CONFIG.kdxTools
      }, _.mapValues(CONFIG.provide, function (v) {
        return JSON.stringify(v);
      }))),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'commons',
        filename: CONFIG.paths.assets.js + 'commons' + (CONFIG.paths.hashes ? '.[hash]' : '') + '.js',
        minChunks: 2
      }),
      new AssetsPlugin({
        filename: 'assets.json',
        path: path.resolve(CONFIG.paths.output),
        prettyPrint: true
      }),
      new CleanWebpackPlugin(CONFIG.paths.output, {
        root: process.cwd(),
        verbose: true,
        dry: false
      })
    ],
    _.map(templatesPaths, function (filepath) {
      var key = _.last(path.parse(filepath).dir.split('/'));
      return new HtmlWebpackPlugin({
        filename: CONFIG.paths.assets.templates + key + '.html',
        template: filepath,
        chunks: ['commons', key]
      })
    }));

  if (CONFIG.devServer.active) {
    plugins.push(new LiveReloadPlugin);
    plugins.push(new webpack.HotModuleReplacementPlugin());
    plugins.push(new webpack.NoEmitOnErrorsPlugin());
  }

  if (CONFIG.minify) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      comments: false,
      compress: {
        drop_console: true
      }
    }));
  }


  return {
    entry: entries,
    output: {
      path: path.resolve(process.cwd(), CONFIG.paths.output),
      publicPath: CONFIG.paths.publicPath,
      filename: CONFIG.paths.assets.js + '[name]' + (CONFIG.paths.hashes ? '.[chunkhash]' : '') + '.js',
      // hotUpdateChunkFilename: CONFIG.paths.assets.js + '[name].[hash].hot.js'
    },
    externals: CONFIG.externals && !!Object.keys(CONFIG.externals).length ? CONFIG.externals : {},
    resolve: {
      extensions: ['.webpack.js', '.web.js', '.js', '.jsx', '.scss', '.ts'],
      alias: aliases
    },
    module: {
      rules: _.concat(LOADERS, sassLoader)
    },
    plugins: plugins,
    devtool: CONFIG.devServer.active ? 'cheap-module-eval-source-map' : false
  }
};




