<a name="0.0.1"></a>
## 0.0.1 (2017-01-20)


### Features

* added core files ([9cdd267](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/9cdd267))
* added eslintignore and some loaders and plugins ([bfd3fcc](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/bfd3fcc))
* created webpack and gulp configurations ([82247ae](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/82247ae))
* created webpack and gulp configurations ([4869d9d](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/4869d9d))
* webpack full config ([f413645](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/f413645))
* webpack modular commit ([9fe4386](https://bitbucket.org/ushakovfedor/babel-boilerplate/commits/9fe4386))



<a name="0.0.1"></a>
## 0.0.1 (2017-01-15)


### Features

* added core files 9cdd267



